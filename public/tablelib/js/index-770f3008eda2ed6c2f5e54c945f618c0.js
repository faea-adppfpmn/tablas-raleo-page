var assign_events,diaginst,on_left_click,on_right_click,update_data
import{Diag}from"./diag-59a5b52d64720b4c15f137585a8f32a8.js"
import{data as test_data}from"./test_data-7ce04c70aa0a28251f1113b756da07e6.js"
diaginst=null,update_data=function(){var t,i,n,a
if($("#diam").html(diaginst.get_sel_diam()),$("#future-diam").html(diaginst.get_sel_future_diam()),$("#weight").html(diaginst.get_sel_weight()),null!=(t=diaginst.get_sel_boxes())){for(i="",n=0,a=t.length;n<a;n++)(t=>i+=t.name+", ")(t[n])
return $("#box").html(i)}return $("#box").html("")},on_left_click=function(){return diaginst.move_left(),update_data()},on_right_click=function(){return diaginst.move_right(),update_data()},assign_events=function(){return $("button#left").on("click",on_left_click),$("button#right").on("click",on_right_click)},$.when($.ready).then(function(){return assign_events(),diaginst=new Diag("diag",test_data),window.diaginst=diaginst})
