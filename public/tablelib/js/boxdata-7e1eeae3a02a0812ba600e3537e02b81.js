var BoxData,BoxManager,STDData
BoxData=function(){class t{constructor(t,a,n){this.min_weight=t,this.max_weight=a,this.name=n}static new_from_json(a){return new t(a.min,a.max,a.name)}}return t.prototype.min_weight=0,t.prototype.max_weight=0,t.prototype.name="70",t}.call(this),STDData=function(){class t{constructor(t){this.name=t,this.data=[]}add(t){return this.data.push(t)}find_box(t){return this.data.find(function(a){return a.min_weight<=t&&t<=a.max_weight})}static new_from_json(a){var n,r,o,e
for(e=new t(a.name),n=0,r=(o=a.boxes).length;n<r;n++)(function(t){var a
a=BoxData.new_from_json(t),e.add(a)})(o[n])
return e}}return t.prototype.name="",t.prototype.data=[],t}.call(this),BoxManager=class{constructor(){this.stds=[]}add(t){return this.stds.push(t)}current_boxes(t){var a
return 0!==(a=this.stds.map(a=>a.find_box(t))).length&&a}load_from_json(t){var a,n,r,o
for(r=[],a=0,n=t.length;a<n;a++)o=t[a],r.push((t=>this.stds.push(STDData.new_from_json(t)))(o))
return r}}
export{BoxData,STDData,BoxManager}
